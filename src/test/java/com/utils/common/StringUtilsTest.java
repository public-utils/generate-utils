package com.utils.common;

import com.utils.enums.CaseEnum;
import org.junit.Test;

/**
 * @author xingxing jiang
 * @create 2021/06/21 10:11
 */
public class StringUtilsTest {

    @Test
    public void convertType() {
        Integer integer = StringUtils.convertType(Integer.class, "11");
        System.out.println(integer);
    }

    @Test
    public void camelToUnderline() {
        String abcABaba = StringUtils.camelToUnderline("abcABaba", CaseEnum.LOWERCASE);
        System.out.println(abcABaba);
    }

    @Test
    public void bigCamelToUnderline() {
        String abcABaba = StringUtils.bigCamelToUnderline("StringUtilsTest", CaseEnum.UPPERCASE);
        System.out.println(abcABaba);
    }

    @Test
    public void underlineToCamel() {
        String abcABaba = StringUtils.underlineToCamel("string_utils_test");
        System.out.println(abcABaba);
    }

}