package com.utils.excel;

import com.utils.excel.entity.SqlExcelRow;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.List;

/**
 * @author xingxing jiang
 * @create 2021/06/21 16:37
 */
public class ExcelUtilsTest {

    @Test
    public void readExcelToEntity() throws Exception {
        String excelPath = "E:\\data\\Workspace\\Java\\javaLearn\\GenerateUtils\\src\\main\\resources\\Excel.xlsx";
        List<SqlExcelRow> result = ExcelUtils.readExcelToEntity(SqlExcelRow.class, new FileInputStream(excelPath), excelPath);

        System.out.println(result);
    }
}