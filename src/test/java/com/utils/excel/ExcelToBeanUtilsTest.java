package com.utils.excel;

import org.junit.Test;

/**
 * @author xingxing jiang
 * @create 2021/06/22 11:48
 */
public class ExcelToBeanUtilsTest {

    @Test
    public void createBean() throws Exception {
        String className = "MybankAccountDigitalwalletAccountbind";  //类名,必填
        String excelPath = "E:\\data\\Workspace\\Java\\javaLearn\\generate-utils\\src\\main\\resources\\Excel2.xlsx";
        String bean = ExcelToBeanUtils.createBean(className, excelPath);
        System.out.println(bean);
    }
}