package com.utils.excel;

import org.junit.Test;

/**
 * @author xingxing jiang
 * @create 2021/06/21 18:25
 */
public class ExcelToSqlUtilsTest {

    @Test
    public void createSql() throws Exception {
        String tableNameDesc = "httpMock请求登记簿";  //表名,可以为空
        String tableName = "HTTP_MOCK_FLOW";  //表名,必填
        String excelPath = "E:\\data\\Workspace\\Java\\javaLearn\\generate-utils\\src\\main\\resources\\Excel.xlsx";
        String sql = ExcelToSqlUtils.createSql(tableName, tableNameDesc, excelPath);
        System.out.println(sql);
    }
}