package com.utils.enums;

/**
 * @author xingxing jiang
 * @create 2021/06/21 10:40
 */
public enum CaseEnum {
    LOWERCASE, UPPERCASE;
}