package com.utils.common;

import com.utils.enums.CaseEnum;
import lombok.extern.java.Log;

import java.math.BigDecimal;

/**
 * 字符操作工具类
 *
 * @author xingxing jiang
 * @create 2021/06/21 9:23
 */
@Log
public final class StringUtils {
    private StringUtils() {
    }

    public static final String LF = System.lineSeparator();

    private static final String UNDERLINE = "_";

    /**
     * 判断字符串是否为空或null
     *
     * @param value 入参字符串
     * @return 是否为空
     */
    public static boolean isEmpty(String value) {
        return null == value || "".equals(value.trim());
    }

    /**
     * 判断字符串是否非空
     *
     * @param value 入参字符串
     * @return 是否非空
     */
    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

    /**
     * 驼峰转下划线
     *
     * @param value 入参字符串
     * @return 下划线格式字符串
     */
    public static String camelToUnderline(String value) {
        return camelToUnderline(value, CaseEnum.UPPERCASE);
    }

    /**
     * 驼峰转下划线
     *
     * @param value    入参字符串
     * @param caseEnum 大小写枚举
     * @return 下划线格式字符串
     */
    public static String camelToUnderline(String value, CaseEnum caseEnum) {
        if (isEmpty(value)) {
            log.warning("[camelToUnderline(String value, CaseEnum caseEnum)] 入参value为空");
            return null;
        }

        if (null == caseEnum)
            caseEnum = CaseEnum.UPPERCASE;

        if (!PatternUtils.isCamelFormat(value)) {
            log.warning("入参value不是驼峰格式，不必转换");
            return value;
        }

        StringBuffer sb = new StringBuffer();
        for (char ch : value.toCharArray()) {
            if (CharUtils.isUpperCase(ch)) {
                sb.append(CharUtils.UNDERLINE);
            }
            sb.append(ch);
        }
        switch (caseEnum) {
            case LOWERCASE:
                return sb.toString().toLowerCase();
            case UPPERCASE:
                return sb.toString().toUpperCase();
            default:
                return null;
        }
    }

    /**
     * 大驼峰转下划线
     *
     * @param value 入参字符串
     * @return 下划线格式字符串
     */
    public static String bigCamelToUnderline(String value) {
        return bigCamelToUnderline(value, CaseEnum.UPPERCASE);
    }

    /**
     * 大驼峰转下划线
     *
     * @param value    入参字符串
     * @param caseEnum 大小写枚举
     * @return 下划线格式字符串
     */
    public static String bigCamelToUnderline(String value, CaseEnum caseEnum) {
        if (isEmpty(value)) {
            log.warning("[bigCamelToUnderline(String value, CaseEnum caseEnum)] 入参value为空");
            return null;
        }

        if (null == caseEnum)
            caseEnum = CaseEnum.UPPERCASE;

        if (!PatternUtils.isBigCamelFormat(value)) {
            log.warning("入参value不是大驼峰格式，不必转换");
            return value;
        }

        StringBuffer sb = new StringBuffer();
        for (char ch : value.toCharArray()) {
            if (CharUtils.isUpperCase(ch)) {
                sb.append(CharUtils.UNDERLINE);
            }
            sb.append(ch);
        }
        switch (caseEnum) {
            case LOWERCASE:
                return sb.substring(1).toString().toLowerCase();
            case UPPERCASE:
                return sb.substring(1).toString().toUpperCase();
            default:
                return null;
        }
    }

    /**
     * 下划线转驼峰
     *
     * @param value 入参字符串
     * @return 驼峰格式字符串
     */
    public static String underlineToCamel(String value) {
        if (isEmpty(value)) {
            log.warning("[underlineToCamel(String value)] 入参value为空");
            return null;
        }

        if (!PatternUtils.isUnderLineFormat(value)) {
            log.warning("入参value不是下划线格式，不必转换");
            return value;
        }
        //是否大写的标识
        boolean flag = false;
        StringBuffer sb = new StringBuffer();
        for (char ch : value.toLowerCase().toCharArray()) {
            if (ch == CharUtils.UNDERLINE) {
                flag = true;
                continue;
            }
            if (flag) {
                sb.append(CharUtils.caseToUpper(ch));
                flag = false;
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    /**
     * 类型转换
     * 将String类型值转换成指定的class类型对象
     *
     * @param clazz 指定的class类型
     * @param value 值
     * @param <T>   返回值类型
     * @return 指定类型对象
     */
    public static <T> T convertType(Class<T> clazz, String value) {
        if (null == clazz || isEmpty(value)) {
            return null;
        }
        if (Integer.class.equals(clazz) || int.class.equals(clazz)) {
            return (T) Integer.valueOf(value);
        }
        if (Byte.class.equals(clazz) || byte.class.equals(clazz)) {
            return (T) Byte.valueOf(value);
        }
        if (Character.class.equals(clazz) || char.class.equals(clazz)) {
            return (T) Character.valueOf(value.charAt(0));
        }
        if (Long.class.equals(clazz) || long.class.equals(clazz)) {
            return (T) Long.valueOf(value);
        }
        if (Float.class.equals(clazz) || float.class.equals(clazz)) {
            return (T) Float.valueOf(value);
        }
        if (Double.class.equals(clazz) || double.class.equals(clazz)) {
            return (T) Double.valueOf(value);
        }
        if (Boolean.class.equals(clazz) || boolean.class.equals(clazz)) {
            return (T) Boolean.valueOf(value);
        }
        if (BigDecimal.class.equals(clazz)) {
            return (T) BigDecimal.valueOf(Long.parseLong(value));
        }
        if (String.class.equals(clazz)) {
            return (T) value;
        }
        return null;
    }
}