package com.utils.common;

import java.util.regex.Pattern;

/**
 * 正则校验工具
 *
 * @author xingxing jiang
 * @create 2021/06/21 9:16
 */
public final class PatternUtils {
    private PatternUtils() {
    }

    /**
     * 驼峰正则
     */
    private static final Pattern CAMEL_PATTERN = Pattern.compile("^[a-z][A-Za-z0-9]*$");
    /**
     * 大驼峰正则
     */
    private static final Pattern BIG_CAMEL_PATTERN = Pattern.compile("^[A-Z][A-Za-z0-9]*$");
    /**
     * 下划线正则
     */
    private static final Pattern UNDERLINE_PATTERN = Pattern.compile("^[A-Za-z]+(_[A-Za-z0-9]+)*$");

    /**
     * 字符串是否为骆峰格式
     *
     * @param value 入参字符串
     * @return 是否为骆峰格式
     */
    public static boolean isCamelFormat(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        return CAMEL_PATTERN.matcher(value).find();
    }

    /**
     * 字符串是否为大骆峰格式
     *
     * @param value 入参字符串
     * @return 是否为大骆峰格式
     */
    public static boolean isBigCamelFormat(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        return BIG_CAMEL_PATTERN.matcher(value).find();
    }

    /**
     * 字符串是否为下划线格式
     *
     * @param value 入参字符串
     * @return 是否为下划线格式
     */
    public static boolean isUnderLineFormat(String value) {
        if (StringUtils.isEmpty(value)) {
            return false;
        }
        return UNDERLINE_PATTERN.matcher(value).find();
    }
}