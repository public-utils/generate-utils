package com.utils.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 时间工具
 *
 * @author xingxing jiang
 * @create 2021/06/21 9:17
 */
public final class DateTimeUtils {
    private DateTimeUtils() {
    }

    private static final DateTimeFormatter DEFAULT_FULL_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


    /**
     * 获取当前日期的yyyy/MM/dd HH:mm:ss格式
     *
     * @return
     */
    public static String formatFullDatetime() {
        return LocalDateTime.now().format(DEFAULT_FULL_FORMATTER);
    }

}