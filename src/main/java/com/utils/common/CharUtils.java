package com.utils.common;

import java.util.Arrays;
import java.util.List;

/**
 * @author xingxing jiang
 * @create 2021/06/21 11:20
 */
public final class CharUtils {
    private CharUtils() {
    }

    public static final char UNDERLINE = '_';
    private static final char EMPTY_CHAR = '\0';
    private static final List<Character> NUMBER_LIST = Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    private static final List<Character> LOWER_CASE_LIST = Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    private static final List<Character> UPPER_CASE_LIST = Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');


    /**
     * 字符是否为小写
     *
     * @param ch 入参字符
     * @return 是否为小写
     */
    public static boolean isLowerCase(char ch) {
        if (ch == EMPTY_CHAR) {
            return false;
        }
        return LOWER_CASE_LIST.contains(ch);
    }

    /**
     * 字符是否为大写
     *
     * @param ch 入参字符
     * @return 是否为大写
     */
    public static boolean isUpperCase(char ch) {
        if (ch == EMPTY_CHAR)
            return false;
        return UPPER_CASE_LIST.contains(ch);
    }

    /**
     * 字符转换成大写
     *
     * @param ch 入参字符
     * @return 大写字符
     */
    public static char caseToUpper(char ch) {
        if (UPPER_CASE_LIST.contains(ch) || !LOWER_CASE_LIST.contains(ch))
            return ch;
        ch -= 32;
        return ch;
    }

    /**
     * 字符转换成小写
     *
     * @param ch 入参字符
     * @return 小写字符
     */
    public static char caseToLower(char ch) {
        if (LOWER_CASE_LIST.contains(ch) || !UPPER_CASE_LIST.contains(ch))
            return ch;
        ch += 32;
        return ch;
    }
}