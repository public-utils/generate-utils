package com.utils.excel.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author xingxing jiang
 * @create 2021/06/21 14:32
 */
@Getter
@Setter
@ToString
public class SqlExcelRow {
    /**
     * 属性列
     */
    private String item;
    /**
     * 注释
     */
    private String annotation;
    /**
     * 类型
     */
    private String type;
    /**
     * 最大长度
     */
    private String maxLength;
    /**
     * 是否必填
     */
    private String isNeed;
    /**
     * 是否为主键
     */
    private String isPrimaryKey;
}