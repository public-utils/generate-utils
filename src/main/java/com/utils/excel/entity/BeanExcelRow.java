package com.utils.excel.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author xingxing jiang
 * @create 2021/06/22 10:38
 */
@Getter
@Setter
@ToString
public class BeanExcelRow {
    /**
     * 属性
     */
    private String field;
    /**
     * 注释
     */
    private String fieldName;
    /**
     * 类型
     */
    private String type;
    /**
     * 最大长度
     */
    private String maxLength;
    /**
     * 是否非空
     */
    private String notNull;
    /**
     * json属性key
     */
    private String jsonField;
}