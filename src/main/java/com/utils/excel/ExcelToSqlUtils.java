package com.utils.excel;

import com.utils.common.StringUtils;
import com.utils.excel.entity.SqlExcelRow;
import lombok.extern.java.Log;

import java.io.FileInputStream;
import java.util.List;

/**
 * 根据excel生成sql
 *
 * @author xingxing jiang
 * @create 2021/06/21 17:24
 */
@Log
public final class ExcelToSqlUtils {
    private ExcelToSqlUtils() {
    }

    /**
     * 生成sql语句
     *
     * @param tableName     表名
     * @param tableNameDesc 表描述
     * @param excelPath     excel文件路径
     * @return
     */
    public static String createSql(String tableName, String tableNameDesc, String excelPath) throws Exception {
        if (StringUtils.isEmpty(tableName)) {
            log.warning("表名不能为空");
            return null;
        }
        if (StringUtils.isEmpty(excelPath)) {
            log.warning("excel文件路径不能为空");
            return null;
        }
        List<SqlExcelRow> rowList;
        try (FileInputStream in = new FileInputStream(excelPath);) {
            rowList = ExcelUtils.readExcelToEntity(SqlExcelRow.class, in, excelPath);
        }
        if (null == rowList) {
            return null;
        }
        String createTableSql = Constants.CREATE_TABLE_SQL;
        String primaryKeySql = Constants.PRIMARY_KEY_SQL;
        StringBuffer itemContentSb = new StringBuffer();                     //具体表字段信息
        StringBuffer annotationContentSb = new StringBuffer();               //后面注释内容
        StringBuffer primaryKeyNameSb = new StringBuffer();                  //主键
        for (SqlExcelRow row : rowList) {
            String itemContentSql = Constants.ITEM_CONTENT_SQL;
            String annotationContentSql = Constants.ANNOTATION_CONTENT_SQL;
            String item = StringUtils.camelToUnderline(row.getItem()).toUpperCase();
            String type = getSqlType(row.getType());
            String annotation = row.getAnnotation();
            String isNeed = isNeed(row.getIsNeed());
            if (isPrimaryKey(row.getIsPrimaryKey()))
                primaryKeyNameSb.append(",").append(item);
            String maxLength = getMaxLength(row.getMaxLength(), type);

            itemContentSql = itemContentSql.replaceAll("<<rowItem>>", item);
            itemContentSql = itemContentSql.replaceAll("<<dataType>>", type);
            itemContentSql = itemContentSql.replaceAll("<<maxLength>>", maxLength);
            itemContentSql = itemContentSql.replaceAll("<<isNeed>>", isNeed);

            annotationContentSql = annotationContentSql.replaceAll("<<tableName>>", tableName);
            annotationContentSql = annotationContentSql.replaceAll("<<item>>", item);
            annotationContentSql = annotationContentSql.replaceAll("<<annotation>>", annotation);

            itemContentSb.append(itemContentSql).append(StringUtils.LF);
            annotationContentSb.append(annotationContentSql).append(StringUtils.LF);
        }
        if (primaryKeyNameSb.length() > 0) {
            primaryKeySql = primaryKeySql.replaceAll("<<tableName>>", tableName);
            primaryKeySql = primaryKeySql.replaceAll("<<primaryKeyName>>", primaryKeyNameSb.substring(1));
        }

        createTableSql = createTableSql.replaceAll("<<tableName>>", tableName);
        createTableSql = createTableSql.replaceAll("<<itemContent>>", itemContentSb.substring(0, itemContentSb.lastIndexOf(",")));
        createTableSql = createTableSql.replaceAll("<<tableNameDesc>>", tableNameDesc);
        createTableSql = createTableSql.replaceAll("<<annotationContent>>", annotationContentSb.toString());
        createTableSql = createTableSql.replaceAll("<<primaryKeyContent>>", primaryKeySql);
        return createTableSql;
    }

    /**
     * 将实体类中type装换成sql的type
     *
     * @param type 实体类中type
     * @return
     */
    private static String getSqlType(String type) {
        if (StringUtils.isEmpty(type))
            return "";
        String realType = Constants.VARCHAR2;
        if (type.contains("int") || type.contains("Num") || type.contains("Int")
                || type.contains("数") || type.contains("num") || type.contains("NUMBER")) {
            realType = Constants.NUMBER;
        } else if (type.toLowerCase().contains("timestamp") || type.contains("datetime")
                || type.contains("时间戳")) {
            realType = Constants.TIMESTAMP;
        } else if (type.toLowerCase().contains("date") || type.contains("日期")) {
            realType = Constants.DATE;
        } else if (type.toLowerCase().contains("time") || type.contains("时间")) {
            realType = Constants.TIME;
        }
        return realType;
    }

    /**
     * 获取最长长度字符串
     *
     * @param length 实体类中length
     * @param type   sql中type
     * @return
     */
    private static String getMaxLength(String length, String type) {
        if (StringUtils.isEmpty(type))
            return "";
        switch (type) {
            case Constants.DATE:
                return "";
            case Constants.VARCHAR2:
                if (StringUtils.isEmpty(length))
                    length = Constants.STRING_MAX_LENGTH;
                return new StringBuffer("(").append(length).append(")").toString();
            case Constants.NUMBER:
                if (StringUtils.isEmpty(length))
                    return "";
                else
                    return new StringBuffer("(").append(length).append(")").toString();
            default:
                return new StringBuffer("(").append(length).append(")").toString();
        }
    }

    /**
     * 是否允许为空
     *
     * @param isNeed sql中是否允许为空
     * @return
     */
    private static String isNeed(String isNeed) {
        if (StringUtils.isNotEmpty(isNeed) && (isNeed.contains("是") || isNeed.contains("true") || isNeed.contains("True")
                || isNeed.contains("Y"))) {
            return Constants.NOT_NULL;
        } else {
            return "";
        }
    }

    private static boolean isPrimaryKey(String isPrimaryKey) {
        if (StringUtils.isNotEmpty(isPrimaryKey) && (isPrimaryKey.contains("是")
                || isPrimaryKey.contains("true") || isPrimaryKey.contains("True") || isPrimaryKey.contains("Y"))) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}