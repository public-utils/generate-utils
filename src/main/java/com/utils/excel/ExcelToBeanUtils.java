package com.utils.excel;

import com.utils.common.DateTimeUtils;
import com.utils.common.StringUtils;
import com.utils.excel.entity.BeanExcelRow;
import lombok.extern.java.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 根据excel生成实体类
 *
 * @author xingxing jiang
 * @create 2021/06/21 17:24
 */
@Log
public final class ExcelToBeanUtils {
    private ExcelToBeanUtils() {
    }

    /**
     * 生成sql语句
     *
     * @param className 类名
     * @param excelPath excel文件路径
     * @return
     */
    public static String createBean(String className, String excelPath) throws Exception {
        if (StringUtils.isEmpty(className)) {
            log.warning("类名不能为空");
            return null;
        }
        if (StringUtils.isEmpty(excelPath)) {
            log.warning("excel文件路径不能为空");
            return null;
        }
        String packagePath = Constants.DEFAULT_PACKAGE_PATH;
        List<BeanExcelRow> rowList;
        try (FileInputStream in = new FileInputStream(excelPath);) {
            rowList = ExcelUtils.readExcelToEntity(BeanExcelRow.class, in, excelPath);
        }
        if (null == rowList) {
            return null;
        }
        String createClassModel = Constants.CREATE_CLASS_MODEL;
        StringBuffer fieldContentSb = new StringBuffer();
        String[] importData = new String[]{"", "", "", ""};
        for (BeanExcelRow row : rowList) {
            String fieldContentModel = Constants.FIELD_CONTENT_MODEL;
            String annotationModel = "";
            String notBlankModel = "";
            String lengthModel = "";
            String jsonfieldModel = "";
            String field = StringUtils.underlineToCamel(row.getField());
            String fieldName = row.getFieldName();
            String type = getJavaType(row.getType(), importData);
            boolean notNull = notNull(row.getNotNull());
            String maxLength = getMaxLength(row.getMaxLength(), type);
            String jsonField = row.getJsonField();

            if (StringUtils.isNotEmpty(fieldName))
                annotationModel = Constants.ANNOTATION_MODEL.replaceAll("<<fieldName>>", fieldName);

            if (notNull) {
                notBlankModel = Constants.NOT_NULL_MODEL.replaceAll("<<fieldName>>", fieldName);
                importData[3] = "import javax.validation.constraints.NotNull;" + StringUtils.LF;
            }

            if (StringUtils.isNotEmpty(maxLength)) {
                lengthModel = Constants.LENGTH_MODEL.replaceAll("<<maxLength>>", maxLength);
                lengthModel = lengthModel.replaceAll("<<fieldName>>", fieldName);
                importData[2] = "import org.hibernate.validator.constraints.Length;" + StringUtils.LF;
            }

            if (StringUtils.isNotEmpty(jsonField)) {
                jsonfieldModel = Constants.JSONFIELD_MODEL.replaceAll("<<jsonField>>", jsonField);
                importData[0] = "import com.alibaba.fastjson.annotation.JSONField;" + StringUtils.LF;
            }


            fieldContentModel = fieldContentModel.replaceAll("<<annotation>>", annotationModel);
            fieldContentModel = fieldContentModel.replaceAll("<<notBlankContent>>", notBlankModel);
            fieldContentModel = fieldContentModel.replaceAll("<<lengthContent>>", lengthModel);
            fieldContentModel = fieldContentModel.replaceAll("<<jsonFieldContent>>", jsonfieldModel);
            fieldContentModel = fieldContentModel.replaceAll("<<fieldType>>", type);
            fieldContentModel = fieldContentModel.replaceAll("<<field>>", field);

            fieldContentSb.append(fieldContentModel);
        }
        createClassModel = createClassModel.replaceAll("<<packagePath>>", packagePath);
        createClassModel = createClassModel.replaceAll("<<importJSONField>>", importData[0]);
        createClassModel = createClassModel.replaceAll("<<importDate>>", importData[1]);
        createClassModel = createClassModel.replaceAll("<<importLength>>", importData[2]);
        createClassModel = createClassModel.replaceAll("<<importNotNull>>", importData[3]);
        createClassModel = createClassModel.replaceAll("<<createDateTime>>", DateTimeUtils.formatFullDatetime());
        createClassModel = createClassModel.replaceAll("<<className>>", className);
        createClassModel = createClassModel.replaceAll("<<fieldContent>>", fieldContentSb.toString());
        File parentFile = new File(System.getProperty("user.dir"), Constants.DEFAULT_FILE_PATH);
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        File outFile = new File(parentFile, new StringBuffer(className).append(".java").toString());
        try (FileOutputStream out = new FileOutputStream(outFile);) {
            out.write(createClassModel.getBytes(StandardCharsets.UTF_8));
        }
        return createClassModel;
    }

    /**
     * 将实体类中type装换成java的type
     *
     * @param type       实体类中type
     * @param importData
     * @return
     */
    private static String getJavaType(String type, String[] importData) {
        if (StringUtils.isEmpty(type))
            return "";
        String realType = Constants.JAVA_STRING;
        if (type.toLowerCase().contains("int") || type.contains("Num")
                || type.contains("数") || type.contains("num") || type.contains("NUMBER")) {
            realType = Constants.JAVA_INT;
        } else if (type.toLowerCase().contains("date") || type.toLowerCase().contains("time")
                || type.contains("日期") || type.contains("时间")) {
            importData[1] = "import java.util.Date;" + StringUtils.LF;
            realType = Constants.JAVA_DATE;
        } else if (type.toLowerCase().contains("byte") || type.toLowerCase().trim().equals("b")) {
            realType = Constants.JAVA_BYTE;
        } else if (type.toLowerCase().contains("short") || type.toLowerCase().trim().equals("s")) {
            realType = Constants.JAVA_SHORT;
        } else if (type.toLowerCase().trim().equals("char") || type.toLowerCase().trim().equals("c")) {
            realType = Constants.JAVA_CHAR;
        } else if (type.toLowerCase().contains("long") || type.toLowerCase().trim().equals("l")) {
            realType = Constants.JAVA_LONG;
        } else if (type.toLowerCase().contains("float") || type.toLowerCase().trim().equals("f")) {
            realType = Constants.JAVA_FLOAT;
        } else if (type.toLowerCase().contains("double") || type.toLowerCase().trim().equals("d")) {
            realType = Constants.JAVA_DOUBLE;
        } else if (type.toLowerCase().contains("bool")) {
            realType = Constants.JAVA_BOOLEAN;
        }
        return realType;
    }

    /**
     * 获取最长长度字符串
     *
     * @param length 实体类中length
     * @param type   java中type
     * @return
     */
    private static String getMaxLength(String length, String type) {
        if (StringUtils.isEmpty(type))
            return "";
        switch (type) {
            case Constants.JAVA_DATE:
                return "";
            default:
                return length.trim();
        }
    }

    /**
     * 是否允许为空
     *
     * @param notNull 是否允许为空
     * @return
     */
    private static boolean notNull(String notNull) {
        if (StringUtils.isNotEmpty(notNull) && (notNull.contains("是") || notNull.contains("true") || notNull.contains("True")
                || notNull.contains("Y"))) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}