package com.utils.excel;

import com.utils.common.StringUtils;
import lombok.extern.java.Log;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author xingxing jiang
 * @create 2021/06/21 15:08
 */
@Log
public final class ExcelUtils {
    private ExcelUtils() {
    }

    /**
     * 校验是否存在且是否为Excel文件
     *
     * @param filePath 文件全路径
     * @return 校验结果
     */
    public static boolean checkFile(String filePath) {
        if (StringUtils.isEmpty(filePath)) {
            log.warning("[checkFile(String filePath)] 入参filePath为空");
            return false;
        }
        return checkFile(new File(filePath));
    }

    /**
     * 校验是否存在且是否为Excel文件
     *
     * @param file 文件对象
     * @return 校验结果
     */
    public static boolean checkFile(File file) {
        if (null == file) {
            log.warning("[checkFile(File file)] 入参file为空");
            return false;
        }
        if (!file.exists()) {
            return false;
        }
        String fileName = file.getName();
        if (StringUtils.isNotEmpty(fileName) && (fileName.endsWith(".xlsx") || fileName.endsWith(".xls"))) {
            return true;
        }
        return false;
    }

    /**
     * 读取excel转换成entity list
     *
     * @param clazz    entity类型
     * @param in       excel流
     * @param filePath excel文件路径
     * @param <T>      类型
     * @return
     */
    public static <T> List<T> readExcelToEntity(Class<T> clazz, InputStream in, String filePath) throws Exception {
        if (!checkFile(filePath)) {
            log.warning("[readExcelToEntity(Class<T> clazz, String filePath)] filePath文件格式校验不通过");
            return null;
        }
        Workbook workbook = getWorkBoot(in, filePath);
        List<T> result = readExcel(clazz, workbook);
        workbook.close();
        return result;
    }

    /**
     * 兼容excel2007以下版本
     * 创建excel workbook对象
     *
     * @param in       excel流
     * @param filePath excel文件路径
     * @return
     */
    private static Workbook getWorkBoot(InputStream in, String filePath) throws IOException {
        if (filePath.endsWith(".xls"))
            return new HSSFWorkbook(in);
        else
            return new XSSFWorkbook(in);
    }

    /**
     * 解析excel 生成实体对象list
     *
     * @param clazz    实体Class对象
     * @param workbook excel workbook对象
     * @param <T>      实体类型
     * @return
     */
    private static <T> List<T> readExcel(Class<T> clazz, Workbook workbook) throws Exception {
        List<T> result = new ArrayList<>(0);
        Sheet sheet;
        int firstRowNum;
        int lastRowNum;
        Row headRow;
        Row dataRow;
        T instance;
        Field field;
        Cell cell;
        Class<?> fieldType;
        for (int sheetNum = 0; sheetNum < workbook.getNumberOfSheets(); sheetNum++) {
            sheet = workbook.getSheetAt(sheetNum);
            firstRowNum = sheet.getFirstRowNum();
            lastRowNum = sheet.getLastRowNum();
            if (firstRowNum == lastRowNum) {
                continue;
            }
            headRow = sheet.getRow(firstRowNum);
            int totalCellNum = headRow.getPhysicalNumberOfCells();
            Field[] fields = clazz.getDeclaredFields();
            if (fields.length > totalCellNum) {
                log.warning("sheet的列数小于对象的属性个数");
                continue;
            }
            for (int rowNum = firstRowNum + 1; rowNum <= lastRowNum; rowNum++) {
                dataRow = sheet.getRow(rowNum);
                if (null == dataRow || isRowEmpty(dataRow))
                    continue;
                instance = clazz.newInstance();
                for (int i = 0; i < fields.length; i++) {
                    field = fields[i];
                    cell = dataRow.getCell(i);
                    if (null == cell)
                        continue;
                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(), clazz);
                    Method writeMethod = pd.getWriteMethod();
                    fieldType = field.getType();
                    if (fieldType.equals(Date.class)) {
                        writeMethod.invoke(instance, cell.getDateCellValue());
                    } else {
                        cell.setCellType(CellType.STRING);
                        writeMethod.invoke(instance, StringUtils.convertType(fieldType, cell.getStringCellValue().trim()));
                    }
                }
                result.add(instance);
            }
        }
        return result;
    }

    /**
     * 判断行是否为空
     *
     * @param row 行对象
     * @return 是否为空
     */
    private static boolean isRowEmpty(Row row) {
        for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
            Cell cell = row.getCell(i);
            if (cell != null && cell.getCellTypeEnum() != CellType.BLANK) {
                return false;
            }
        }
        return true;
    }
}