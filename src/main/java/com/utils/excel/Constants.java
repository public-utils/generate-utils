package com.utils.excel;

import com.utils.common.StringUtils;

/**
 * @author xingxing jiang
 * @create 2021/06/21 14:37
 */
public interface Constants {
    String CREATE_TABLE_SQL = new StringBuffer(" CREATE TABLE <<tableName>> ").append(StringUtils.LF)
            .append("(").append(StringUtils.LF)
            .append("<<itemContent>>  ").append(StringUtils.LF)
            .append(") CHARSET utf8;").append(StringUtils.LF)
            .append("COMMENT ON TABLE <<tableName>> IS '<<tableNameDesc>>';").append(StringUtils.LF)
            .append(StringUtils.LF)
            .append("<<annotationContent>>").append(StringUtils.LF)
            .append("<<primaryKeyContent>>")
            .toString();

    String ITEM_CONTENT_SQL = "<<rowItem>>  <<dataType>><<maxLength>> <<isNeed>>,";

    String ANNOTATION_CONTENT_SQL = "COMMENT ON COLUMN <<tableName>>.<<item>> is '<<annotation>>';";

    String PRIMARY_KEY_SQL = "ALTER TABLE <<tableName>> ADD PRIMARY KEY (<<primaryKeyName>>);";


    //oracle数据类型相关
    String NUMBER = "NUMBER";
    String VARCHAR2 = "VARCHAR2";
    String TIMESTAMP = "TIMESTAMP";
    String DATE = "DATE";
    String TIME = "TIME";


    String NOT_NULL = "not null";
    String STRING_MAX_LENGTH = "256";//默认字符串最大位数

    String DEFAULT_PACKAGE_PATH = "com.utils.beans";

    String DEFAULT_FILE_PATH = "src/main/resources/beans";

    //java类型相关
    String JAVA_STRING = "String";
    String JAVA_DATE = "Date";
    String JAVA_BYTE = "byte";
    String JAVA_SHORT = "short";
    String JAVA_CHAR = "char";
    String JAVA_INT = "int";
    String JAVA_LONG = "long";
    String JAVA_FLOAT = "float";
    String JAVA_DOUBLE = "double";
    String JAVA_BOOLEAN = "boolean";

    String CREATE_CLASS_MODEL = new StringBuffer("package <<packagePath>>;").append(StringUtils.LF)
            .append(StringUtils.LF)
            .append("<<importJSONField>>")
            .append("import lombok.Getter;").append(StringUtils.LF)
            .append("import lombok.Setter;").append(StringUtils.LF)
            .append("import lombok.ToString;").append(StringUtils.LF)
            .append("<<importDate>>")
            .append("<<importLength>>")
            .append("<<importNotNull>>")
            .append(StringUtils.LF)
            .append("/**").append(StringUtils.LF)
            .append(" * @author xingxing jiang").append(StringUtils.LF)
            .append(" * @create <<createDateTime>>").append(StringUtils.LF)
            .append(" */").append(StringUtils.LF)
            .append("@Getter").append(StringUtils.LF)
            .append("@Setter").append(StringUtils.LF)
            .append("@ToString").append(StringUtils.LF)
            .append("public class <<className>> {").append(StringUtils.LF)
            .append("<<fieldContent>>")
            .append("}").toString();

    String FIELD_CONTENT_MODEL = new StringBuffer("<<annotation>>")
            .append("<<notBlankContent>>")
            .append("<<lengthContent>>")
            .append("<<jsonFieldContent>>")
            .append("\tprivate <<fieldType>> <<field>>;")
            .append(StringUtils.LF).toString();

    String ANNOTATION_MODEL = new StringBuffer("\t/**").append(StringUtils.LF)
            .append("\t * <<fieldName>>").append(StringUtils.LF)
            .append("\t */")
            .append(StringUtils.LF).toString();

    String NOT_NULL_MODEL = new StringBuffer("\t@NotNull(message = \"<<fieldName>>不能为空\")")
            .append(StringUtils.LF).toString();
    String LENGTH_MODEL = new StringBuffer("\t@Length(max = <<maxLength>>, message = \"<<fieldName>>最大长度不能超过<<maxLength>>\")")
            .append(StringUtils.LF).toString();
    String JSONFIELD_MODEL = new StringBuffer("\t@JSONField(name = \"<<jsonField>>\")")
            .append(StringUtils.LF).toString();

}